﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;

namespace RateOnline
{
    class Program
    {
        static void Main(string[] args)
        {
// Текущая дата
            string data = string.Empty;
            // Адрес сайта с курсом валюты
            string url = "http://www.nbrb.by/statistics/rates/ratesDaily.asp?fromdate=";

            // HTML сайта с курсом валюты

            string html = string.Empty;

            // Регулярное выражение

            string pattern = "доллар США</td><td>(.*)</td></tr><tr ><td>EUR";
            
            // Определяем текущую дату
            DateTime today = DateTime.Now;
            data = today.Date.ToShortDateString();
            Console.WriteLine(data);
            // Формируем адрес сайта

            url += data; 
            // Отправляем GET запрос и получаем в ответ HTML-код сайта с курсом валюты
            HttpWebRequest  myHttpWebRequest  = (HttpWebRequest)HttpWebRequest.Create(url);
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            StreamReader myStreamReader = new StreamReader(myHttpWebResponse.GetResponseStream());
            html = myStreamReader.ReadToEnd();
            // Вытаскиваем из HTML-кода нужные данные
            Match match = Regex.Match(html, pattern);
            Console.WriteLine("Курс Доллара США на {0} равен {1} руб.", data, match.Groups[1].ToString());
            Console.ReadLine();

        }
    }
}
